import React from 'react';
import Header from './Header/Header'
import Home from './Pages/Players/PlayersDisplay/Home'
import Player from './Pages/Players/Players'
import UpdatePlayer from './Pages/Players/Forms/UpdateForm'
import { Container } from 'react-bootstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";
function Dashboard(props) {
    return (
        <>
            <Router>
            <Container>
            <Header></Header>
            <div>
                <Switch>
                <Route exact path="/">
                    <Home></Home>
                </Route>
                <Route path="/player">
                    <h1>PLAYER DATA</h1>
                    <Player></Player>
                </Route>
                <Route path="/update/1">
                    <h1>UPDATE DATA</h1>
                    <UpdatePlayer>dd</UpdatePlayer>
                </Route>
                </Switch>
            </div>
            </Container>
            </Router>
        </>
    )
}
export default Dashboard;