import TablePlayers from './PlayersDisplay/TablePlayers'
import {Table} from 'react-bootstrap';
import { Form, Button} from 'react-bootstrap';
// import Forms from './Forms/component/Form'
import db from '../../DataPlayer'
import React, { Component } from 'react'

export default class Players extends Component {
    constructor(props) {
        super(props)

        this.state = {
            players: db,
            newPlayer: {
                username:'',
                password:'',
                email:'',
                experience:'',
                lvl:''
           }
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }

    handleSubmit(e){
    e.preventDefault();
    
      this.setState((prevState) => {
        console.log(prevState)
        const objectBaru = { 
            ...prevState,
            players: prevState.players.concat({
                username:this.state.newPlayer.username,
                email:this.state.newPlayer.email,
                password:this.state.newPlayer.password,
                experience:this.state.newPlayer.experience,
                lvl:this.state.newPlayer.lvl
            })
        }
        
        return objectBaru;
        })
        
    }

      handleInput(e, element) {
        const { newPlayer } = this.state;
        newPlayer[element] = e.target.value;
        this.setState({ newPlayer });
      }

    render() {
    const { players, newPlayer } = this.state;
    const { username, email,password,experience, lvl} = newPlayer;
        return (
            <>
        {/* <Form onSubmit={this.handleSubmit}>   MASIH ERROR
            <Forms controlId="username" label="Username" type="text" placeholder="username" value={username} onChange={e => this.handleInput(e, 'username')}></Forms>
            <Forms controlId="email" label="Email" type="email" value={email} onChange={e => this.handleInput(e, 'email')}></Forms>
            <Forms controlId="password" label="Password" type="password" value={password} onChange={e => this.handleInput(e, 'password')}></Forms>
            <Forms controlId="experience" label="Expereience" type="number" value={experience} onChange={e => this.handleInput(e, 'experience')}></Forms>
            <Forms controlId="lvl" label="Level" type="number" value={lvl} onChange={e => this.handleInput(e, 'lvl')}></Forms>
            <Button variant="primary" type="submit">Submit</Button>
        </Form> */}
        <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId="username">
                <Form.Label>Username</Form.Label>
                <Form.Control className="col-sm-3" type="text" value={username} onChange={e => this.handleInput(e, 'username')}/>
            </Form.Group>
            <Form.Group controlId="email">
                <Form.Label>Email</Form.Label>
                <Form.Control className="col-sm-3" type="email" value={email} onChange={e => this.handleInput(e, 'email')}/>
            </Form.Group>
            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control className="col-sm-3" type="password" placeholder="Password" value={password} onChange={e => this.handleInput(e, 'password')}/>
            </Form.Group>            
            <Form.Group controlId="experience">
                <Form.Label>Experience</Form.Label>
                <Form.Control className="col-sm-3" type="number" value={experience} onChange={e => this.handleInput(e, 'experience')}/>
            </Form.Group>
            <Form.Group controlId="lvl">
                <Form.Label>Level</Form.Label>
                <Form.Control className="col-sm-3" type="number" value={lvl} onChange={e => this.handleInput(e, 'lvl')}/>
            </Form.Group>
            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>
        <h2>Exsiting Players:</h2>
        <Table striped bordered hover size="sm">
            <thead>
                <tr>
                <th>No</th>
                <th>Username</th>
                <th>Email</th>
                <th>Password</th>
                <th>Experience</th>
                <th>Level</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {
                    players.map((el)=>{
                        return <TablePlayers username={el.username} email={el.email} password={el.password} experience={el.experience} lvl={el.lvl} />
                    })
                }
            </tbody>
        </Table>
             
        </>
        )
    }
}
