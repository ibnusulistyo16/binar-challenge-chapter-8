import React from 'react'
import {Button} from 'react-bootstrap';

function Tableplayers(props) {
    
    return (
        <>
            <tr>
                    <th>{props.id}</th>
                    <th>{props.username}</th>
                    <th>{props.email}</th>
                    <th>{props.password}</th>
                    <th>{props.experience}</th>
                    <th>{props.lvl}</th>
                    <th><td>
                        <Button href="/update/1">Update</Button>
                        </td>
                    </th>
                </tr> 
        </>
    )
}

export default Tableplayers;
