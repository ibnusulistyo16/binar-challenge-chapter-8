import { Form} from 'react-bootstrap';
import React, { Component } from 'react'

export default class FormInput extends Component {
    constructor(props) {
        super(props)

        this.state = {
        }
    }

    render() {
        return (
            <>
            <Form.Group controlId={this.props.controlId}>
                    <Form.Label>{this.props.label}</Form.Label>
                    <Form.Control className="col-sm-3" type={this.props.type}  placeholder={this.props.placeholder} value={this.props.value}/>
            </Form.Group>
            </>
        )
    }
}
