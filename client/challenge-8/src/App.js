import Dashboard from './Dashboard/Dashboard';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <Dashboard/>
    </div>
  );
}

export default App;
