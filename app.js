const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const swaggerUI = require('swagger-ui-express');
const swaggerJSON = require('./swagger.json');
const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// accept request in form or JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//documentation swagger
app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));

const db = require("./app/models");
db.client.sync();

require("./app/routes/player.routes")(app);


//error handling
app.use((req,res, next)=>{
  const error = new ("Not Found!");
  error.status=404;
  next(error);
})
app.use((error, req, res, next)=>{
  res.status(error.status|| 500);
  res.json({
      error:{
          message:error.message,
      }
  })
})

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
